const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const Employee = mongoose.model('employee');
var employee = new Employee();

router.get('/add', (req, res) => {
        // Employee.find((err, doc) => {
        //     if (!err)
        //     res.send(doc);
        //         // res.render('employee/list', {
        //         //     list: doc
        //         // });
        //     else {
        //         console.log("Error=>" + err)
        //     }
        // });
        res.render('course/index', {title: "add"});
    }
);

router.post('/create', (req, res) => {
        employee.fullname = req.body.fullname;
        employee.mobile = req.body.mobile;
        employee.city = req.body.city;
        // console.log(employee);
        // employee.save((err, doc) => {
        //     console.log(doc);
        //     if (!err)
        //         res.redirect('/employee');
        //     else {
        //         console.log("Error=>" + err);
        //     }
        //
        // });


        // Save Note in the database
        employee.save()
            .then(data => {
                // res.send(data);
                res.redirect('/employee');
            }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Note."
            });
        });
    }
);

router.get('/', (req, res) => {
        // Retrieve and return all notes from the database.
        // exports.findAll = (req, res) => {
        //     employee.find()
        //         .then(employee => {
        //             res.send(employee);
        //         }).catch(err => {
        //         res.status(500).send({
        //             message: err.message || "Some error occurred while retrieving notes."
        //         });
        //     });
        // };
        Employee.find((err, doc) => {
            if (!err)
            // res.send(doc);
                res.render('employee/list', {
                    list: doc,
                    title: "Employee"
                });
            else {
                console.log("Error=>" + err);
            }
        })
    }
);


module.exports = router;
