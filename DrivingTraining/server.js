require('./models/db');
const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const bodyparser = require('body-parser');


const employeeController = require('./controllers/employeeController');
const courseController = require('./controllers/courseController');
var app = express();
app.use(bodyparser.urlencoded({
    extended: true
}));
app.use(bodyparser.json());

app.set('views', path.join(__dirname, '/views/'));
app.engine('hbs', exphbs({extname: 'hbs', defaultLayout: 'main', layoutsDir: __dirname + '/views/layouts/'}));
app.set('view engine', 'hbs');

app.listen(3000, () => {
    console.log("Express server started ar port: 3000");
});

app.use('/employee', employeeController);
app.use('/course', courseController);


//path for css
app.use(express.static(path.join(__dirname, 'assets')));
app.use(express.static(path.join(__dirname, 'script')));

